# Notify Module
The "Notify" module provides a common code base related to user notification. Currently the only notification method
available is "email".

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-notify.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-notify/get/master.zip](http://code.tsstools.com/bower-notify/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-notify](http://code.tsstools.com/bower-notify)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.notify']);
```

## To Do
- Implement additional notification methods.
- Port over email creation directives from the Road Analzyer project.