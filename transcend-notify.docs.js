/**
 * @ngdoc overview
 * @name transcend.notify
 * @description
 # Notify Module
 The "Notify" module provides a common code base related to user notification. Currently the only notification method
 available is "email".

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-notify.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-notify/get/master.zip](http://code.tsstools.com/bower-notify/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-notify](http://code.tsstools.com/bower-notify)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.notify']);
 ```

 ## To Do
 - Implement additional notification methods.
 - Port over email creation directives from the Road Analyzer project.
 */
/**
 * @ngdoc service
 * @name transcend.notify.service:Email
 *
 * @description
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} that provides the ability to
 * construct and send emails - either as a POST or GET request.
 *
 <pre>
 Email.sendPost({to:'me@mail.com', subject:'subject', body:'hello'});
 </pre>
 *
 * @requires nnResource.$resource
 * @requires tsConfig
 *
 * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
 * details on what methods are natively available to a resource.
 */
/**
     * @ngdoc method
     * @name transcend.notify.service:Email#send
     * @methodOf transcend.notify.service:Email
     *
     * @description
     * Sends an email via a GET request.
     *
     * @param {object} params The email parts, such as from, to, subject, body, etc.
     *
     * @example
     <pre>
     Email.send({to:'me@mail.com', subject:'subject', body:'hello'});
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.notify.service:Email#sendPost
     * @methodOf transcend.notify.service:Email
     *
     * @description
     * Authenticates a user account with a set of credentials (user name and password) using an HTTP POST request.
     *
     * @param {object} params The email parts, such as from, to, subject, body, etc.
     *
     * @example
     <pre>
     Email.sendPost({to:'me@mail.com', subject:'subject', body:'hello'});
     </pre>
     */
